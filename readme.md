# The Doctor Scrolls Slicerym

The Doctor Scrolls Slicerym is a radiology educational game that aims at helping doctors to preview catscans in an augmented reality environment as well as test there abilities to correctly position catscan slices on a 3D model.  
The game has 2 modes:

* Educational mode
* Game mode

## 1. Educational Mode:
The user can interact with the 3D models and preview the catsacns updated in real time and the user has the ability to slide through the model to view different catscans.

## 2. Game Mode:
The user can test his ability to place a catscan slice on a 3D model in the correct place trying to obtain the highest score in a time constrained environment and incresed difficulty of not knowing the orientation of the slice on the 3D model.

***

### Developer Notes (to be deleted later on upon game release):

[comment]: <> (:white_medium_square: emoji for unchecked box)
[comment]: <> (:ballot_box_with_check: emoji for checked box)

1. Critical Features

    * :white_medium_square: ~~Projection and viewing planes of catscans~~
    * :white_medium_square: ~~Slice position mapping from images to 3D model position~~
    * :white_medium_square: ~~Sonification~~
    * :white_medium_square: ~~Timer~~
    * :white_medium_square: ~~Gesture control~~
    * :white_medium_square: Changing between modes of coronal, axial, sagittal
    

2. Secondary Features
    * :white_medium_square: Oblique CT slices
    * :white_medium_square: ~~Menu/UI~~
    * :white_medium_square: ~~Scoring system~~
    * :white_medium_square: ~~Slice placement~~
    * :white_medium_square: Data preparation
    
3. Nice To Have Features
    * :white_medium_square: ~~Model placement~~
    * :white_medium_square: ~~Mode selection~~
    * :white_medium_square: ~~Voice command control~~
    * :white_medium_square: Quiz
    
4. Bug Fixes
    * :white_medium_square: 1- voice commands for game mode
    * :white_medium_square: 2- better ui 
    * :white_medium_square: 3- projections menu
    * :white_medium_square: 4- game mode placement script
    * :white_medium_square: 5- check loading time
    * :white_medium_square: 6- ambient music
    * :white_medium_square: 7- fix anatomy orientation
    * :white_medium_square: 8- code refactoring
